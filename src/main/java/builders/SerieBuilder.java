package builders;

import java.util.*;

import domain.ChartSerie;
import domain.Point;
import domain.SerieType;


public class SerieBuilder {

	private ChartSerie chartSetting;
	
	public SerieBuilder(){	
		chartSetting = new ChartSerie();
		
	}
	
	public SerieBuilder addPoints( Point point ){
		
		List<Point> p = chartSetting.getPoints();
		p.add(point);
		return this;
		
	}
	
	public SerieBuilder addLabel( String label ){
		
		chartSetting.setLabel( label );
		return this;
		
	}
	
	public SerieBuilder withPoints( List<Point> list ){
		
		chartSetting.setPoints( list );
		return this;
		
	}
	
	public SerieBuilder setType( SerieType type ){
		
		chartSetting.setSerieType( type );
		return this;
		
	}
	
	public ChartSerie build(){
		
		return chartSetting;
		
	}
}
