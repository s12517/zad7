package builders;

import java.util.*;

import domain.ChartSerie;
import domain.ChartSetting;
import domain.ChartType;

public class ChartBuilder {

	private ChartSetting chartSetting;
	
	public ChartBuilder(){		
		chartSetting = new ChartSetting();		
	}
	
	public ChartBuilder addSerie( ChartSerie serie ){		
		List<ChartSerie> list = chartSetting.getSeries();
		list.add(serie);			
		return this;
	}
	
	public ChartBuilder withSeries( List<ChartSerie> list ){		
		chartSetting.setSeries( list );	
		return this;
	}
	
	public ChartBuilder withTitle( String title ){		
		chartSetting.setTitle( title );
		return this;
		
	}
	
	public ChartBuilder withLegend(){		
		chartSetting.setHaveLegend( true );
		return this;
		
	}
	
	public ChartBuilder withType( ChartType type ){		
		chartSetting.setChartType( type );
		return this;
		
	}
	
	public ChartSetting build(){		
		return chartSetting;
		
	}
}