package domain;

public enum ChartType {
	
	Line, Point, LinePoint, Bar, StackedBar, Area, Pie

}