package tests;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;
import builders.SerieBuilder;
import domain.ChartSerie;
import domain.Point;
import domain.SerieType;

public class SerieBuilderTest {

private SerieBuilder serieBuilder;
	
	@Before
	public void before(){
		serieBuilder=new SerieBuilder();
		
	}
	
    @Test
    
    public void addPointsTest(){
    	
    	//GIVEN
    	Point point = new Point(1,1);
    	
    	//WHEN
       	ChartSerie chartSerie = serieBuilder.addPoints(point).build();
    	
    	//THEN
    	assertThat(chartSerie.getPoints()).contains(point);
    	
    }
    

	@Test
	public void addLabelTest(){
		
		//GIVEN
		String label = "etykieta";
		
		//WHEN
		ChartSerie chartSerie = serieBuilder.addLabel(label).build();
		
		//THEN
		assertThat(chartSerie.getLabel()).isEqualTo(label);
	}
	

	@Test
	public void withPointsTest(){
		
		// GIVEN
		List<Point> point = new ArrayList<Point>();
					point.add( new Point( 1,1 ) );
					point.add( new Point( 1,2 ) );
					point.add( new Point( 1,3 ) );
					point.add( new Point( 1,4 ) );
				
		// WHEN
		ChartSerie chartSerie = serieBuilder.withPoints(point).build();
					
		// THEN
		assertThat( chartSerie.getPoints()).hasSize(4);
	}
	
	@Test
	public void setTypeTest(){
		
					
		// GIVEN
		SerieType type = SerieType.Bar;
		
		// WHEN 
		ChartSerie chartSerie = serieBuilder.setType(type).build();
			
		// THEN
			assertThat(chartSerie.getSerieType()).isEqualTo(type);
	}
	
}

