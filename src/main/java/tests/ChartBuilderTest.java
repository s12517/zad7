package tests;

import java.util.List;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;
import builders.ChartBuilder;
import domain.ChartSerie;
import domain.ChartSetting;
import domain.ChartType;

public class ChartBuilderTest {
	
	private ChartBuilder chartBuilder;
	
	@Before
	public void before(){
		chartBuilder = new ChartBuilder();
		
	}
	
	@Test
	public void addSerieTest() {
		
		// GIVEN
		String title = "seria";
		ChartSerie chartSerie = new ChartSerie( );
		chartSerie.setLabel(title);
		
		// WHEN
		ChartSetting chartSetting = chartBuilder.addSerie(chartSerie).build();
		
		// THEN
		assertThat(chartSetting.getSeries()).isEqualTo(title);
	}
	
	@Test
	public void withSeriesTest(){
		
		// GIVEN
		List<ChartSerie> list = new ArrayList<ChartSerie>();
			list.add(new ChartSerie());
			list.add(new ChartSerie());
			list.add(new ChartSerie());
			list.add(new ChartSerie());
		
		// WHEN
		ChartSetting chartSetting = chartBuilder.withSeries(list).build();
		
		// THEN
		assertThat(chartSetting.getSeries()).hasSize(4);
	}
	
	@Test
	public void withTitleTest(){
		
		// GIVEN
		String title = "tytul";
		
		// WHEN
		ChartSetting chartSetting = chartBuilder.withTitle(title).build();
		
		// THEN
		assertThat(chartSetting.getTitle()).isEqualTo(title);
	}
	
	@Test
	public void withLegendTest(){
		
		// WHEN
		ChartSetting chartSetting = chartBuilder.withLegend().build();
		
		// THEN
		assertThat(chartSetting.isHaveLegend()).isTrue();
	}

	@Test
	public void withTypeTest(){
		
		// GIVEN
		ChartType chartType = ChartType.Pie;
		
		// WHEN
		ChartSetting chartSetting = chartBuilder.withType(chartType).build();
		
		// THEN
		assertThat(chartSetting.getCharType()).isEqualTo(chartType);
	}
}